 

Docker, Docker Compose & Gradle


First Microservices in Docker
In this task, you will need to create a multi module gradle project, and create automation tasks to build, package, and deploy the microservices in docker.

Gradle Multi Module Project
First create a gradle project with two independent sub modules

1.	Create a git repo, and create a gradle project with 2 sub modules
2.	Name the first submodule ms1
3.	Create a hello (GET) api under ms1
4.	Run the ms1 module, and open the hello api. Makse sure “Hello from ms1 ”+ count,  is returned as a message from the api.
5.	Name the second submodule ms2
6.	Create a hello api under ms2
7.	Run the ms2 GET)  module, and open the hello api. Makse sure “Hello from ms2 ”+count is returned as a message from the api.
8.	Count is increased by one each time we call the API
9.	Make sure count is stored in the database
10.	The database should be in docker (docker-compose)
11.	Make sure you have configured database volumes correctly, so that when we delete all docker containers the counter value is not lost

Build docker images with Gradle
1.	Create necessary tasks in gradle to build and push docker images from each submodule
2.	You should add the tasks from root build gradle to all sub projects
3.	Once tasks are created, navigate to root project and call the tasks so that docker images are built for each submodule and pushed to docker hub (create a docker hub account for yourself) 
4.	For versioning, you can use git-tag, so when ever you change anything in the code and commit it to git, the tag is changed
. 
Deploy the modules
1.	Create a docker-compose yaml in the root of the project
2.	Add the submodules to docker compose, and call docker compose up to run the submodules
3.	Add nginx reverse proxy to the docker-compose file
4.	Expose the reverse proxy on some port like 8000
5.	Configure the nginx in such a way that, when we call http://localhost:8000/ms1/hello it returns message from the ms1, and when we call  http://localhost:8000/ms2/hello it returns the message from ms2 respectively

Deploy using Gradle 
1.	Finally, create a task in gradle
2.	The task should build the submodules, create and push docker images, and replace the docker image versions in docker compose yml and run the new code in docker


For discussions, questions use the slack channel.
 

Best of Luck!

