package az.task.ms1.controller;

import az.task.ms1.services.CountServiceImpl;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/ms1-hello")
public class HelloApiMs1 {

    private  final CountServiceImpl countServiceImp;

    public HelloApiMs1(CountServiceImpl countServiceImp) {
        this.countServiceImp = countServiceImp;
    }

    @GetMapping
    String getMessage(){

        countServiceImp.incrementCount();
        return "Hello Ms-1 count :"+countServiceImp.getCountValue();
    }

}
