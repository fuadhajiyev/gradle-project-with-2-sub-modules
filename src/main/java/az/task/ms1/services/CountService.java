package az.task.ms1.services;

import az.task.ms1.domen.Count;

public interface CountService {

    int incrementCount();
}
