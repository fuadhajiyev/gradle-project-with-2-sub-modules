package az.task.ms1.services;


import az.task.ms1.domen.Count;
import az.task.ms1.repositories.CountRepository;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

@Service
public class CountServiceImpl implements CountService {

    private final CountRepository countRepository;

    public CountServiceImpl(CountRepository countRepository) {
        this.countRepository = countRepository;
    }

    @Transactional
    @Override
    public int incrementCount() {
        Count count = getCount();
        count.setCount(count.getCount() + 1);
        countRepository.save(count);
        return count.getCount();
    }


    @Transactional
    public int getCountValue() {
        Count count = getCount();
        return count.getCount();
    }


    private Count getCount() {
        return countRepository.findById(1L).orElse(new Count());
    }
}
