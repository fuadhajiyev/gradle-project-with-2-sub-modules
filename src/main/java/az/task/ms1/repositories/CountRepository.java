package az.task.ms1.repositories;

import az.task.ms1.domen.Count;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CountRepository extends JpaRepository<Count, Long> {
//    Optional<Count> findById(long l);
}
