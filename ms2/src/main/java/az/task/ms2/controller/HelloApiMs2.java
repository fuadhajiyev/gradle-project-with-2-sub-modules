package az.task.ms2.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/m2-hello")
public class HelloApiMs2 {


    @GetMapping
    String getMessage(){

        return "Hello Ms-2";
    }

}
