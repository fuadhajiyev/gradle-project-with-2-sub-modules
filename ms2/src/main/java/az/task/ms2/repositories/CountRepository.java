package az.task.ms2.repositories;

import az.task.ms2.domen.Count;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CountRepository extends JpaRepository<Count,Long> {
}
