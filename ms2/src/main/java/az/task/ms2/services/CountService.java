package az.task.ms2.services;

import az.task.ms2.domen.Count;

public interface CountService {

    Count incrementCount();
}
