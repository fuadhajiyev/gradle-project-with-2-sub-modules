FROM alpine
COPY  build/libs/ms1-0.0.1-SNAPSHOT.jar /app/ms1-0.0.1-SNAPSHOT.jar
RUN apk add --no-cache openjdk17
WORKDIR "/app"
ENTRYPOINT ["java"]
CMD ["-jar", "ms1-0.0.1-SNAPSHOT.jar"]